package com.devcamp.bookauthorapi.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.bookauthorapi.models.Book;
import com.devcamp.bookauthorapi.services.BookService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class BookController {
    @Autowired
    BookService bookService;

    @GetMapping("/books")
    public ArrayList<Book> getAllBooks() {
        ArrayList<Book> allBook =bookService.getAllBooks();
        return allBook;
    }

    @GetMapping("/book-quantity")
    public ArrayList<Book> getBooksByQuantity(@RequestParam(name="quantityNumber", required = true) int quantityNumber){
        ArrayList<Book> findBook = bookService.getBooksByQuantity(quantityNumber);
        return findBook;
    }

    
    @GetMapping("/books/{bookId}")
    public Book getBookByIndex(@PathVariable("bookId") int bookId){
        Book findBook = bookService.getBookByIndex(bookId);
        return findBook;
    }
}
