package com.devcamp.bookauthorapi.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.bookauthorapi.models.Author;

@Service
public class AuthorService {
    Author author1 = new Author("An", "a@gmail.com", 'M');
    Author author2 = new Author("Bi", "b@gmail.com", 'M');
    Author author3 = new Author("Du", "d@gmail.com", 'M');
    Author author4 = new Author("Minh", "m@gmail.com", 'F');
    Author author5 = new Author("Hieu", "hieu@gmail.com", 'M');
    Author author6 = new Author("Mai", "mai@gmail.com", 'F');

    public Author[] authorList1() {
        ArrayList<Author> arrAuthor = new ArrayList<Author>();
        arrAuthor.add(author1);
        arrAuthor.add(author2);
        Author[] authorArray = arrAuthor.toArray(new Author[0]);
        return authorArray;
    }
    
    public Author[] authorList2() {
        ArrayList<Author> arrAuthor = new ArrayList<Author>();
        arrAuthor.add(author4);
        arrAuthor.add(author3);
        Author[] authorArray = arrAuthor.toArray(new Author[0]);
        return authorArray;
    }
    public Author[] authorList3() {
        ArrayList<Author> arrAuthor = new ArrayList<Author>();
        arrAuthor.add(author5);
        arrAuthor.add(author6);
        Author[] authorArray = arrAuthor.toArray(new Author[0]);
        return authorArray;
    }

    public ArrayList<Author> getAllAuthor(){
        ArrayList<Author> arrAuthor = new ArrayList<Author>();

        arrAuthor.add(author1);
        arrAuthor.add(author2);
        arrAuthor.add(author3);
        arrAuthor.add(author4);
        arrAuthor.add(author5);
        arrAuthor.add(author6);

        return arrAuthor;
    }

    public Author getAuthorByEmail(String email) {
        ArrayList<Author> arrAuthor = getAllAuthor();

        for (Author author : arrAuthor) {
            if (author.getEmail().equals(email)) {
                return author;
            }
        }
        return null;
    }

    public ArrayList<Author> getAuthorsByGender(Character gender) {
        ArrayList<Author> arrAuthor = getAllAuthor();
        ArrayList<Author> result = new ArrayList<>();
        for (Author author : arrAuthor) {
            if (author.getGender() == gender) {
                result.add(author);
            }
        }
        return result;
    }
}
